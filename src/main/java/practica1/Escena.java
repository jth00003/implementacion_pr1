/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;
import static practica1.Fotograma.ID;
import static practica1.PRACTICA.generador;
import static practica1.Utilidades.MIN_NUM_FOTOGRAMAS;
import static practica1.Utilidades.TIEMPO_FINALIZACION_ESCENA;
import static practica1.Utilidades.VARIACION_NUM_FOTOGRAMAS;


/**
 *
 * @author Javier
 */
public class Escena {    
    static int ID=0;
    private final String idE;
    private int duracion;
    private boolean prioridad;
    private Date horaGeneracion;
    private Date horaInicioProcesamiento;
    private Date horaFinProcesamiento;
    private List<Fotograma> listaFotogramas;    
    
    public Escena( ) throws InterruptedException{                 
        idE=""+ID++;       
        duracion= TIEMPO_FINALIZACION_ESCENA;
        int tipo= generador.nextInt(2);
        prioridad= ( tipo==0 ) ;
        this.listaFotogramas=new LinkedList<>();        
        horaInicioProcesamiento= new Date();
        horaFinProcesamiento= new Date();        
   } 
   
   public void inicializaEscena(){
        setHoraGeneracion(new Date());  
       int numFotogramasAGenerar;
       numFotogramasAGenerar= MIN_NUM_FOTOGRAMAS+generador.nextInt(VARIACION_NUM_FOTOGRAMAS);        
       for (int i=0; i<numFotogramasAGenerar; i++){
           Fotograma fotog = new Fotograma();
            this.listaFotogramas.add(fotog);
            setDuracion(getDuracion() + fotog.getDuracion());
       }
       
   }
   
    public String getIdE() {
        return idE;
    }
    
    public int getDuracion() {
        return duracion;
    }
    
    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }
    
    public boolean isPrioridad() {
        return prioridad;
    }
    
    public void setPrioridad(boolean prioridad) {
        this.prioridad = prioridad;
    }
    
    public Date getHoraGeneracion() {
        return horaGeneracion;
    }
    
    public void setHoraGeneracion(Date horaGeneracion) {
        this.horaGeneracion = horaGeneracion;
    }
    
    public Date getHoraInicioProcesamiento() {
        return horaInicioProcesamiento;
    }
    
    public void setHoraInicioProcesamiento(Date horaInicioProcesamiento) {
        this.horaInicioProcesamiento = horaInicioProcesamiento;
    }
    
    public Date getHoraFinProcesamiento() {
        return horaFinProcesamiento;
    }
    
    public void setHoraFinProcesamiento(Date horaFinProcesamiento) {
        this.horaFinProcesamiento = horaFinProcesamiento;
    }

    public List<Fotograma> getListaFotogramas() {
        return listaFotogramas;
    }

    public void setListaFotogramas(List<Fotograma> listaFotogramas) {
        this.listaFotogramas = listaFotogramas;
    }
    
    public float TotalProcesamiento(){
        return (horaFinProcesamiento.getTime()-horaInicioProcesamiento.getTime())/1000;
    }
    public String toString(){
        String cad= new String();
        cad= "Escena { \n" + "    hora generacion=" + horaGeneracion +"\n" +
                "    hora comienzo Render.=" + horaInicioProcesamiento + "\n" +
                "    hora fin Render.=" + horaFinProcesamiento  +"\n" +
                "    Tiempo Total proceso=" + TotalProcesamiento()
                 + " }";
        for (Fotograma foto:listaFotogramas){
            cad=cad+ "\n" + "   --> " +foto.toString() ;
        }
        cad=cad+"\n";
        
        return cad;
    }

}
