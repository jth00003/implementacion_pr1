/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;


import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Javier
 */
public class Finalizador implements Runnable{
    List<Renderizador> listaRenderizadores;
    
    public Finalizador( LinkedList<Renderizador> listaRenderizadores ){
        this.listaRenderizadores= listaRenderizadores;
    }
    public void run(){
        listaRenderizadores.forEach((ren) -> {
            ren.setTerminadosGeneradores();
        });
    }
    
    
}

