/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;


import static practica1.PRACTICA.generador;
import static practica1.Utilidades.INC_TIEMPO_FOTOGRAMA;
import static practica1.Utilidades.MIN_TIEMPO_FOTOGRAMA;

/**
 *
 * @author Javier
 */
public class Fotograma {
    static int ID=0;
    private final String idF;
    private int duracion;
    
    public Fotograma(  ){
        idF=""+ID++;
        duracion= MIN_TIEMPO_FOTOGRAMA+generador.nextInt(INC_TIEMPO_FOTOGRAMA);        
    }

    public String getIdF() {
        return idF;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }
    
    @Override
     public String toString(){
         return "Fotograma { id: " + idF + " duracion: " + duracion +"} ";
     }
}

