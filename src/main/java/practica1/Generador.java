/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;


import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import static practica1.PRACTICA.generador;
import static practica1.Utilidades.VARIACION_NUM_ESCENAS;
import static practica1.Utilidades.VARIACION_TIEMPO_GENERACION;
import static practica1.Utilidades.MIN_NUM_ESCENAS;
import static practica1.Utilidades.MIN_TIEMPO_GENERACION;


/**
 *
 * @author Javier
 */
public class Generador implements Callable<List<Escena>>{
    static int ID=0;    
    String idG;
    List<Escena> resultado;
    List<Escena> listaAltaPrioridad;
    Semaphore fillSemAltaPrioridad;
    Semaphore mutexAltaPrioridad;
    Semaphore emptySemAltaPrioridad;
    List<Escena> listaBajaPrioridad;
    Semaphore fillSemBajaPrioridad;
    Semaphore mutexBajaPrioridad;
    Semaphore emptySemBajaPrioridad;
    CyclicBarrier barrera;
    Finalizador finalizador;
    
public Generador( Semaphore fillSemAltaPrioridad, 
                Semaphore emptySemAltaPrioridad,
                List<Escena> listaAltaPrioridad,
                Semaphore fillSemBajaPrioridad, 
                Semaphore emptySemBajaPrioridad,
                List<Escena> listaBajaPrioridad, 
                Semaphore mutexAltaPrioridad,
                Semaphore mutexBajaPrioridad,
                CyclicBarrier barrera, Finalizador finalizador){
    idG=""+ID++;        
    this.resultado=new LinkedList<>();
    this.fillSemAltaPrioridad= fillSemAltaPrioridad;
    this.mutexAltaPrioridad= mutexBajaPrioridad;
    this.emptySemAltaPrioridad = emptySemAltaPrioridad;
    this.listaAltaPrioridad= listaAltaPrioridad;
    this.fillSemBajaPrioridad= fillSemBajaPrioridad;
    this.mutexBajaPrioridad= mutexAltaPrioridad;
    this.emptySemBajaPrioridad = emptySemBajaPrioridad;
    this.listaBajaPrioridad= listaBajaPrioridad;
    this.barrera= barrera;
    this.finalizador= finalizador;
   
}

    @Override
    @SuppressWarnings("empty-statement")
   public List<Escena> call() throws Exception  {
    int numEscenasAGenerar= MIN_NUM_ESCENAS+generador.nextInt(VARIACION_NUM_ESCENAS);
    System.out.println("Se ha iniciado la ejecuciÃ³n de"+this.toString()+"");
    try{
        for (int i=0; i<numEscenasAGenerar; i++){
            int tiempoGeneracionEscena= MIN_TIEMPO_GENERACION+generador.nextInt(VARIACION_TIEMPO_GENERACION);  
            try {
                TimeUnit.SECONDS.sleep(tiempoGeneracionEscena);
            } catch (InterruptedException ex) {
                Logger.getLogger(Generador.class.getName()).log(Level.SEVERE, null, ex);
            }
            Escena escena= new Escena();
            escena.inicializaEscena();
            resultado.add(escena);
            if (escena.isPrioridad() == true){
                    fillSemAltaPrioridad.acquire();;
                    mutexAltaPrioridad.acquire();
                    System.out.println("Inserta escena "+escena.getIdE()+" de alta prioridad");
                    listaAltaPrioridad.add(escena);
                    mutexAltaPrioridad.release();            
                    emptySemAltaPrioridad.release();

            }else{          
                    fillSemBajaPrioridad.acquire();;
                    mutexBajaPrioridad.acquire();
                    System.out.println("Inserta escena "+escena.getIdE()+" de baja prioridad");
                    listaBajaPrioridad.add(escena);
                    mutexBajaPrioridad.release(); 
                    emptySemBajaPrioridad.release();
            }        
        }
        if (barrera.getNumberWaiting()==0) {
           System.out.println("HAAAA");        
           finalizador.run();
        }                                       
        this.barrera.await();
        
   }catch(InterruptedException e ){ 
        System.out.println("Se ha interrumpido la ejecuciÃ³n de"+this.toString()+"");
   } finally {
     
        System.out.println("Ha finalizado la ejecuciÃ³n de "+this.toString()+"");
        return resultado;
   }   
  
}
    @Override
    public String toString(){
        return "Generador{ " + "id=" + idG + " }";
    }
    
}
