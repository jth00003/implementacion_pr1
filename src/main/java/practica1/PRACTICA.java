/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import static practica1.Utilidades.MAX_ESCENAS_EN_ESPERA;
import static practica1.Utilidades.NUM_GENERADORES;

/**
 *
 * @author Javier
 */
public class PRACTICA {

     public final static Random generador = new Random();
     
        static Semaphore fillSemAltaPrioridad;
        static Semaphore emptySemAltaPrioridad;
        static List<Escena> listaAltaPrioridad;
        static Semaphore fillSemBajaPrioridad; 
        static Semaphore emptySemBajaPrioridad;
        static List<Escena> listaBajaPrioridad;
        static Semaphore mutexAltaPrioridad;
        static Semaphore mutexBajaPrioridad;
    
    public static void main(String[] args) throws InterruptedException, ExecutionException, BrokenBarrierException, TimeoutException {
        
        System.out.println("Ha iniciado la ejecuciÃ³n el Hilo(PRINCIPAL)");
        inicializarVariablesCompartidas();
        ExecutorService ejecucion = (ExecutorService) Executors.newCachedThreadPool();              
        
        CyclicBarrier barrera= new CyclicBarrier(NUM_GENERADORES);
        Future<List<Escena>>  escenasIni = null;
     
         
    LinkedList<Renderizador> renderizadores = new LinkedList();
      for (int i = 0; i < NUM_GENERADORES; i++) {
          Renderizador ren=new Renderizador(fillSemAltaPrioridad, 
                                       emptySemAltaPrioridad,
                                       listaAltaPrioridad,
                                       fillSemBajaPrioridad, 
                                       emptySemBajaPrioridad,
                                       listaBajaPrioridad,
                                        mutexAltaPrioridad,
                                        mutexBajaPrioridad);
        renderizadores.add(ren);
       
    }  
    Finalizador finalizador= new Finalizador(renderizadores);
   LinkedList<Generador> generadores = new LinkedList();
   for (int i = 0; i < NUM_GENERADORES; i++){                   

       escenasIni = ejecucion.submit(new Generador(fillSemAltaPrioridad, 
                                    emptySemAltaPrioridad,
                                    listaAltaPrioridad,
                                    fillSemBajaPrioridad, 
                                    emptySemBajaPrioridad,
                                    listaBajaPrioridad,
                                    mutexAltaPrioridad,
                                    mutexBajaPrioridad, barrera, finalizador));
    } 
   List<Future<List<Escena>>> escenasFin = ejecucion.invokeAll(renderizadores); 
    ejecucion.shutdown();
    ejecucion.awaitTermination(10, TimeUnit.SECONDS);
        
        
    float total=0f;
    int totalEscenas=0;
    for (Future<List<Escena>> escena : escenasFin) {
             List<Escena> lista=escena.get();
             totalEscenas+=lista.size();
             for(Escena datos:lista){
                 total+=datos.TotalProcesamiento();
                 System.out.println("La escena "+datos.getIdE()+" generÃ³ los siguientes datos :"+datos.toString());
             }
         }
    System.out.println("TotalEscenas: "+ totalEscenas);
    System.out.println("TotalProcesamiento: "+ total );
    System.out.println("Finalizaron todos los procesos");  
    System.out.println("Ha finalizado la ejecuciÃ³n el Hilo(PRINCIPAL)");

}
    
    static void inicializarVariablesCompartidas() {
    
        fillSemAltaPrioridad= new Semaphore(MAX_ESCENAS_EN_ESPERA); 
        emptySemAltaPrioridad= new Semaphore(0);
        listaAltaPrioridad= new LinkedList<>();
        fillSemBajaPrioridad= new Semaphore(MAX_ESCENAS_EN_ESPERA); 
        emptySemBajaPrioridad= new Semaphore(0);;
        listaBajaPrioridad= new LinkedList<>();
        mutexAltaPrioridad= new Semaphore(1);
        mutexBajaPrioridad= new Semaphore(1);
    }
            
    
}

