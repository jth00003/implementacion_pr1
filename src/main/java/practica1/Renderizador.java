/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
 

/**
 *
 * @author Javier
 */
public class Renderizador implements Callable<List<Escena>>{
    public static final int RETARDO = 1;
    
    static int ID=0;
    private final String idR;
    private boolean terminadosGeneradores;
    List<Escena> resultado;
    Semaphore mainMutex;
    List<Escena> listaAltaPrioridad;
    Semaphore fillSemAltaPrioridad;
    Semaphore mutexAltaPrioridad;
    Semaphore emptySemAltaPrioridad;
    List<Escena> listaBajaPrioridad;
    Semaphore fillSemBajaPrioridad;
    Semaphore mutexBajaPrioridad;
    Semaphore emptySemBajaPrioridad;
    
  public Renderizador(Semaphore fillSemAltaPrioridad, 
                Semaphore emptySemAltaPrioridad,
                List<Escena> listaAltaPrioridad,
                Semaphore fillSemBajaPrioridad, 
                Semaphore emptySemBajaPrioridad,
                List<Escena> listaBajaPrioridad,
                Semaphore mutexAltaPrioridad,
                Semaphore mutexBajaPrioridad){
    idR=""+ID++;
    terminadosGeneradores= false; 
    this.resultado=new LinkedList<>();
    this.mainMutex = new Semaphore(1);
    this.fillSemAltaPrioridad= fillSemAltaPrioridad;
    this.emptySemAltaPrioridad = emptySemAltaPrioridad;
    this.listaAltaPrioridad= listaAltaPrioridad;
    this.fillSemBajaPrioridad= fillSemBajaPrioridad;
    this.emptySemBajaPrioridad = emptySemBajaPrioridad;
    this.listaBajaPrioridad= listaBajaPrioridad;
    this.mutexAltaPrioridad= mutexAltaPrioridad;
    this.mutexBajaPrioridad= mutexBajaPrioridad;
  }

  public List<Escena> call() throws InterruptedException {
    Escena escena= null;
    boolean tengoEscena= false;
    while (true){
        tengoEscena= false;
        mainMutex.acquire();
        if (!listaAltaPrioridad.isEmpty()){
            emptySemAltaPrioridad.acquire();
            mutexAltaPrioridad.acquire();
            escena= listaAltaPrioridad.get(0);
            listaAltaPrioridad.remove(0);
            mutexAltaPrioridad.release();
            fillSemAltaPrioridad.release();
            tengoEscena= true;
        }else{
            if (!listaBajaPrioridad.isEmpty()){
                emptySemBajaPrioridad.acquire();
                mutexBajaPrioridad.acquire();
                escena= listaBajaPrioridad.get(0);
                listaBajaPrioridad.remove(0);
                mutexBajaPrioridad.release();
                fillSemBajaPrioridad.release();
                tengoEscena= true;
            }else{
                if(terminadosGeneradores){
                    mainMutex.release();
                    return resultado;
                }
            }
        }
        mainMutex.release();
        if (tengoEscena){
            System.out.println("Procesando Escena "+escena.getIdE());
            escena.setHoraInicioProcesamiento(new Date());
            TimeUnit.SECONDS.sleep(escena.getDuracion());
            escena.setHoraFinProcesamiento(new Date());
            resultado.add(escena);
        }else{
            TimeUnit.SECONDS.sleep(RETARDO);
        }        
    }
      
  }
  
  public void setTerminadosGeneradores(){
      terminadosGeneradores= true;
  }

}
