/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;

/**
 *
 * @author Javier
 */
public interface Utilidades {
 
    public static final int MIN_TIEMPO_FOTOGRAMA = 2;
    public static final int INC_TIEMPO_FOTOGRAMA = 2;
    
    public static final int MIN_NUM_FOTOGRAMAS = 3;
    public static final int VARIACION_NUM_FOTOGRAMAS = 3;
    
    public static final int MIN_NUM_ESCENAS = 3;
    public static final int VARIACION_NUM_ESCENAS = 3;
    
    public static final int MIN_TIEMPO_GENERACION = 3;
    public static final int VARIACION_TIEMPO_GENERACION = 2;
    
    public static final int MAX_ESCENAS_EN_ESPERA = 4;
    
    public static final int NUM_GENERADORES = 4;
    public static final int NUM_RENDERIZADORES = 3;
    
    public static final int TIEMPO_FINALIZACION_ESCENA = 2;
    
    
}

